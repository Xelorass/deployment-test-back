# Spring Boot JPA MySQL - Building Rest CRUD API example

## Install

- Faites "java -version". Il faut que ce soit un JDK (et non un JRE). Si cela ne fonctionne pas, installez java : https://download.oracle.com/java/17/latest/jdk-17_windows-x64_bin.exe
- Installer maven : https://dlcdn.apache.org/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.zip (unzip dans un répertoire de dev), ajouter la variable M2_HOME et ajouter au PATH (cf :https://howtodoinjava.com/maven/how-to-install-maven-on-windows/).
- Tester si maven fonctionne : "mvn --version"
- Installer Mysql 8 si ce n'est pas déjà fait
- lancer "mvn install" dans le répertoire
- Modifier le fichier : src/main/resources/application.properties

## Run Spring Boot application

```
mvnw spring-boot:run
```

- Tester l'app : http://localhost:8080

## Docker

- Ouvrir un terminal à la racine du repo local back
- lancer pour créer l'image (le nom présenté est important car elle est utilisé dans le compose)

```
sh docker build -t deploy-solution-back .
```

- puis

```
sh docker run --name back-spring-boot -e "DATABASE_HOST=***" -e "DATABASE_NAME=***" -e "DATABASE_USERNAME=***" -e "DATABASE_PASSWORD=***" -p 8088:8080 deploy-solution-back
```

Le container est à présent lancé et accessible via "localhost:8088".

### Docker compose

- Ouvrir un terminal à la racine du repo local back

```
sh docker compose -f docker_compose.yml up
```

La création prend beaucoup de temps et il arrive parfois de prendre un timeout. Relancer la commande et ça sera bon.

#### Prérequis

- Avoir pull les repo front et back dans le même dossier
- Avoir créer les images "deploy-solution-front" et "deploy-solution-back"

Tout fonctionne correctement chez moi j'espère que vous n'aurez pas de problème de votre côté !
