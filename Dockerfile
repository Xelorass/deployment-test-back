FROM openjdk:8-jdk-alpine

COPY target/mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/mds-dds-exam-spring-boot-server-0.0.1-SNAPSHOT.jar"]